#!/bin/bash

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Music.desktop ]]; then
	flatpak override --user --unshare=network org.gnome.Music
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Totem.desktop ]]; then
	flatpak override --user --unshare=network org.gnome.Totem
	flatpak override --filesystem=/media org.gnome.Totem --user
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop ]]; then
	flatpak override --user --env=SIGNAL_USE_WAYLAND=1 org.signal.Signal --socket=wayland
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop ]]; then
	flatpak override --user --env=SIGNAL_USE_TRAY_ICON=1 org.signal.Signal
fi
