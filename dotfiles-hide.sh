#!/bin/bash

if [[ ! -d ~/.local/share/applications ]]; then
	mkdir ~/.local/share/applications
fi

declare -a apps=("nm-connection-editor" "syncthing-start" "syncthing-ui" "kyodialog9.3")

for app in ${apps[@]}; do 
	if [[ -f /usr/share/applications/$app.desktop && ! -f ~/.local/share/applications/$app.desktop ]]; then
		echo "Hiding $app"

		cp /usr/share/applications/$app.desktop ~/.local/share/applications

		echo "NoDisplay=true" | tee -a ~/.local/share/applications/$app.desktop
	fi
done
