#!/bin/bash

show_nonfree_flatpaks(){
	for flatpak in $(flatpak list --all --columns=ref | tail -n +1); do 
		if [[ "$(LANG=en flatpak info "$flatpak" | grep License)" == "     License: LicenseRef-proprietary" ]]; then
			echo "$flatpak is not free"
		fi
	done
}

show_unverified_flatpaks(){
	for url in $(flatpak list --columns=origin,ref | grep -v Platform | grep -v Sdk | grep -v Gtk3theme | sed 's|/.*||' | sed 's|.*\t|https://flathub.org/apps/|'); do
		content=$(curl -s $url)

		if [[ -z $(echo $content | grep "aria-label=\"This app is verified\"") && -z $(echo $content | grep "404 - Page not found") && -z $(echo $content | grep "500 - An error occurred on the server.") ]]; then
			echo "$url is not verified"
		fi
	done
}

usage() {
	echo ""
	echo "Options:"
	echo " -n - show non-free Flatpaks. Optional."
	echo " -u - show unverified Flatpaks. Optional."
	echo " -h - display this help text."
	echo ""
}

while getopts 'nuh' OPTION; do
	case "$OPTION" in
		n)
			show_nonfree_flatpaks
			;;
		u)
			show_unverified_flatpaks
			;;
		h)
			usage
			exit 0
			;;
		*)
			usage
			exit 1
			;;
	esac
done
