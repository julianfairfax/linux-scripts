#!/bin/bash

Real_Path()
{
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#.}"
}

cd "$(Real_Path "${0%/*}")"

for file in ./*; do
	if [[ $file == ./install*.sh ]]; then
		bash $file
	fi
done
