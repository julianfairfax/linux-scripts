#!/bin/bash

if [[ -d "$HOME/Personal" ]]; then
	backup_folder="$HOME/Personal/Backup/$(hostname)"
else
	backup_folder="$HOME/Backup/$(hostname)"
fi

if [[ -z "$(which hostname)" ]]; then
	backup_folder="$HOME/Backup"
fi

if [[ ! -z "$(which apt)" ]]; then
	sudo cp "$backup_folder"/sources.list /etc/apt/

	for file in "$backup_folder"/sources.list.d/*; do
		if [[ $file == *.list ]]; then
			sudo cp -R $file /etc/apt/sources.list.d
		fi
	done

	if [[ -f "$backup_folder"/trusted.gpg ]]; then
		sudo cp "$backup_folder"/trusted.gpg /etc/apt/
	fi

	for file in "$backup_folder"/trusted.gpg.d/*; do
		if [[ $file == *.gpg ]]; then
			sudo cp -R $file /etc/apt/trusted.gpg.d
		fi
	done

	for file in "$backup_folder"/keyrings/*; do
		if [[ $file == *.gpg ]]; then
			sudo cp -R $file /usr/share/keyrings
		fi
	done

	sudo apt update

	sudo apt install $(cat "$backup_folder"/packages.txt | sed 's/\n/ /g' -z)
fi

if [[ ! -z "$(which pacman)" ]]; then
	sudo tar -xjvf "$backup_folder"/pacman_database.tar.bz2

	sudo cp "$backup_folder"/pacman.conf /etc/

	sudo pacman -Syy

	sudo pacman -S --needed $(comm -12 <(pacman -Slq | sort) <(sort "$backup_folder"/packages.txt))
fi

if [[ ! -z "$(which snap)" ]]; then
	while read snap; do
		echo "snap install "$(echo "$snap" | sed 's/\ /\ --/')"" | sudo /bin/bash
	done < "$backup_folder"/snap.txt
fi

if [[ ! -z "$(which flatpak)" ]]; then
	sudo cp "$backup_folder"/*.trustedkeys.gpg /var/lib/flatpak/repo/

	sudo cp "$backup_folder"/config /var/lib/flatpak/repo/

	while read origin ref; do
		flatpak install --assumeyes $origin $ref
	done < "$backup_folder"/flatpak.txt
fi

if [[ ! -z "$(which dnf)" ]]; then
	for file in "$backup_folder"/yum.repos.d/*; do
		if [[ $file == *.repo ]]; then
			sudo cp -R $file /etc/yum.repos.d
		fi
	done

	for file in "$backup_folder"/rpm-gpg/*; do
		sudo cp -R $file /etc/pki/rpm-gpg
	done

	sudo dnf install $(cat "$backup_folder"/dnf.txt | sed 's/\n/ /g' -z)
fi
