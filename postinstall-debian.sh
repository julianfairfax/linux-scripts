#!/bin/bash

sudo apt install cups flatpak gnome-console gnome-disk-utility gnome-shell nautilus

sudo systemctl enable avahi-daemon.service cups.service cups-browsed.service fstrim.timer gdm.service NetworkManager.service

sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

sudo flatpak install --noninteractive net.nokyan.Resources org.gnome.baobab org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.clocks org.gnome.Connections org.gnome.Contacts org.gnome.Epiphany org.gnome.Evince org.gnome.font-viewer org.gnome.Logs org.gnome.Loupe org.gnome.Maps org.gnome.Music org.gnome.SimpleScan org.gnome.Snapshot org.gnome.TextEditor org.gnome.Weather org.gnome.World.PikaBackup
