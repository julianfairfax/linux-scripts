#!/bin/bash

if [[ -d "$HOME/Personal" ]]; then
	backup_folder="$HOME/Personal/Backup/$(hostname)"
else
	backup_folder="$HOME/Backup/$(hostname)"
fi

if [[ -z "$(which hostname)" ]]; then
	backup_folder="$HOME/Backup"
fi

if [[ -d "$backup_folder" ]]; then
	rm -rf "$backup_folder"
	mkdir -p "$backup_folder"
else
	mkdir -p "$backup_folder"
fi

if [[ -d /etc/apt/sources.list.d ]]; then
	mkdir "$backup_folder"/sources.list.d
fi

if [[ -d /etc/apt/trusted.gpg.d ]]; then
	mkdir "$backup_folder"/trusted.gpg.d
fi

if [[ -d /usr/share/keyrings ]]; then
	mkdir "$backup_folder"/keyrings
fi

if [[ -d /etc/yum.repos.d ]]; then
	mkdir "$backup_folder"/yum.repos.d
fi

if [[ ! -z "$(which apt)" ]]; then
	sudo apt clean

	sudo apt purge --autoremove

	dpkg --get-selections | sed 's/\t.*//' > "$backup_folder"/all.txt

	apt-mark showmanual > "$backup_folder"/packages.txt

	cp /etc/apt/sources.list "$backup_folder"/

	for file in /etc/apt/sources.list.d/*; do
		if [[ $file == *.list ]]; then
			cp -R $file "$backup_folder"/sources.list.d
		fi
	done

	if [[ -f /etc/apt/trusted.gpg ]]; then
		cp /etc/apt/trusted.gpg "$backup_folder"/
	fi

	for file in /etc/apt/trusted.gpg.d/*; do
		if [[ $file == *.gpg ]]; then
			cp -R $file "$backup_folder"/trusted.gpg.d
		fi
	done

	for file in /usr/share/keyrings/*; do
		if [[ $file == *.gpg ]]; then
			cp -R $file "$backup_folder"/keyrings
		fi
	done
fi

if [[ ! -z "$(which pacman)" ]]; then
	sudo pacman --noconfirm -Sc

	pacman -Qdtq | sudo pacman --noconfirm -Rs -

	pacman -Qqen > "$backup_folder"/packages.txt

	pacman -Qqem > "$backup_folder"/local.txt

	cp /etc/pacman.conf "$backup_folder"/

	tar -cjf "$backup_folder"/pacman_database.tar.bz2 /var/lib/pacman/local
fi

if [[ ! -z "$(which snap)" ]]; then
	snap list | tail -n +2 | sed 's/\ .*//' | tee /tmp/snap_names.txt &>/dev/null
	snap list | tail -n +2 | sed 's/.*latest\///' | sed 's/\ .*//' | tee /tmp/snap_tracking.txt &>/dev/null

	paste -d "\ " /tmp/snap_names.txt /tmp/snap_tracking.txt | tee "$backup_folder"/snap.txt &>/dev/null

	rm /tmp/snap_names.txt
	rm /tmp/snap_tracking.txt
fi

if [[ ! -z "$(which flatpak)" ]]; then
	flatpak uninstall --unused

	flatpak list --columns=origin,ref | tail -n +1 > "$backup_folder"/flatpak.txt

	cp /var/lib/flatpak/repo/config "$backup_folder"/

	cp /var/lib/flatpak/repo/*.trustedkeys.gpg "$backup_folder"/
fi

if [[ ! -z "$(which dnf)" ]]; then
	dnf list --installed | sed 's/\..*//' | tee "$backup_folder"/dnf.txt

	for file in /etc/yum.repos.d/*; do
		if [[ $file == *.repo ]]; then
			cp -R $file "$backup_folder"/yum.repos.d
		fi
	done
fi

if [[ ! -z "$(which check-dfsg-status)" ]]; then
	check-dfsg-status | tee "$backup_folder"/check-dfsg-status.txt
fi

if [[ ! -z "$(which vrms-rpm)" ]]; then
	vrms-rpm | tee "$backup_folder"/vrms-rpm.txt
fi
